var open = require('open');
var express = require('express');
var app = express();
var bodyParser = require('body-parser')

app.use(express.static('../../'));
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/signature', function (req, res, next) {
    res.redirect(307, 'http://localhost:8080/cotton-signatory/signature');
});

app.post('/store', function (req, res, next) {
    res.redirect(307, 'http://localhost:8080/cotton-signatory/store');
});

app.get('/file/:id', function (req, res, next) {
    res.redirect(302, 'http://mydomain.com:8080/cotton-signatory/file?id=' + req.params["id"]);
});

var server = app.listen(8888, function () {
    var host = server.address().address;
    var port = server.address().port;

    open('http://localhost:8888/cotton-signatory/demo/');

    console.log('Example app listening at http://%s:%s', host, port);
});